import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export const constantRoutes = [{
        path: '/',
        component: () =>
            import ('../views/Home.vue'),
        hidden: true
    },
    {
        path: '/Login',
        component: () =>
            import ('../views/Login/Login.vue'),
        hidden: true
    },
    {
        path: '/About',
        component: () =>
            import ('../views/About.vue'),
        hidden: true
    },
    {
        path: '/Product',
        component: () =>
            import ('../views/Product/Product.vue'),
        hidden: true
    },
    {
        path: '/NewProduct',
        component: () =>
            import ('../views/Product/NewProduct.vue'),
        hidden: true
    }
]
const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router